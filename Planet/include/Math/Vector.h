#pragma once
#include <math.h>
#include "Physics.h"

template<typename T> class TVector2 {
public:
    T x,y;

    TVector2()                      { this->Set(0,0); }
    TVector2(const T x, const T y)  { this->Set(x,y); }
    TVector2(const T *lp)           { this->Set(lp); }
    TVector2(const TVector2<T> &v)  { this->Set(v); }

    void Set(T x, T y)             { this->x = x; this->y = y; }
    void Set(T *lp)                { this->Set(lp[0],lp[1]); }
    void Set(const TVector2<T> &v) { this->Set(v.x,v.y); }

    // unary operators
    operator TVector2<float>()            const { return TVector2<float>((float)this->x, (float)this->y); }
    operator TVector2<double>()           const { return TVector2<double>((double)this->x, (double)this->y); }
    const T operator [] (const int index) const { return ((T*)this)[index]; }
          T operator [] (const int index)       { return ((T*)this)[index]; }
    TVector2<T>* operator *()                   { return ((TVector2<T>*) this); }
    const TVector2<T> operator -()        const { return TVector2<T>(-x,-y); }

    
    // vector assignment operators
    TVector2<T>& operator  = (const TVector2<T> &rhs)   { this->Set(rhs); return *this; }
    TVector2<T>& operator += (const TVector2<T> &rhs)   { this->x += rhs.x; this->y += rhs.y; return *this; }
    TVector2<T>& operator -= (const TVector2<T> &rhs)   { this->x -= rhs.x; this->y -= rhs.y; return *this; }
    TVector2<T>& operator *= (const TVector2<T> &rhs)   { this->x *= rhs.x; this->y *= rhs.y; return *this; }
    TVector2<T>& operator /= (const TVector2<T> &rhs)   { this->x /= rhs.x; this->y /= rhs.y; return *this; }

    // scalar assignment operators
    TVector2<T>& operator  = (const T rhs)              { this->Set(rhs,rhs); return *this; }
    TVector2<T>& operator += (const T rhs)              { this->x += rhs; this->y += rhs; return *this; }
    TVector2<T>& operator -= (const T rhs)              { this->x -= rhs; this->y -= rhs; return *this; }
    TVector2<T>& operator *= (const T rhs)              { this->x *= rhs; this->y *= rhs; return *this; }
    TVector2<T>& operator /= (const T rhs)              { this->x /= rhs; this->y /= rhs; return *this; }

    // binary vector operators
    const TVector2<T> operator + (const TVector2<T> &rhs)   const { return TVector2<T>(*this) += rhs; }
    const TVector2<T> operator - (const TVector2<T> &rhs)   const { return TVector2<T>(*this) -= rhs; }
    const TVector2<T> operator * (const TVector2<T> &rhs)   const { return TVector2<T>(*this) *= rhs; }
    const TVector2<T> operator / (const TVector2<T> &rhs)   const { return TVector2<T>(*this) /= rhs; }

    // binary scalar operators
    const TVector2<T> operator + (const T rhs)              const { return TVector2<T>(*this) += rhs; }
    const TVector2<T> operator - (const T rhs)              const { return TVector2<T>(*this) -= rhs; }
    const TVector2<T> operator * (const T rhs)              const { return TVector2<T>(*this) *= rhs; }
    const TVector2<T> operator / (const T rhs)              const { return TVector2<T>(*this) /= rhs; }

    TVector2<T>& Normalize()                                  { *this /= (T)this->Magnitude(); return *this; }

    double Sum()                                        const { return this->x + this->y; }
    double Dot(const TVector2<T> &v)                    const { return TVector2<T>(*this * v).Sum(); }
    double SquareMag()                                  const { return this->Dot(*this); }
    double Magnitude()                                  const { return sqrt(this->SquareMag()); }
    double SquareDelta(const TVector2<T> &v)            const { return (*this - v).SquareMag(); }
    double Delta(const TVector2<T> &v)                  const { return sqrt(this->SquareDelta(v)); }

    const TVector2<T> Midpoint(const TVector2<T> &v)    const { return TVector2<T>((*this - v) / 2 + v); }
    const TVector2<T> Average(const TVector2<T> &v)     const { return TVector2<T>((*this + v) / 2); }
};

template<typename T> class TVector3 {
public:
    T x,y,z;

    TVector3()                                  { this->Set(0,0,0); }
    TVector3(const T x, const T y, const T z)   { this->Set(x,y,z); }
    TVector3(const T *lp)                       { this->Set(lp); }
    TVector3(const TVector2<T> &v)              { this->Set(v); }
    TVector3(const TVector3<T> &v)              { this->Set(v); }

    void Set(const T x, const T y, const T z)   { this->x = x; this->y = y; this->z = z; }
    void Set(const T *lp)                       { this->Set(lp[0], lp[1], lp[2]); }
    void Set(const TVector2<T> &v)              { this->Set(v.x,v.y,  0); }
    void Set(const TVector3<T> &v)              { this->Set(v.x,v.y,v.z); }

    // unary operators
    operator TVector3<float>()                        const { return TVector3<float>((float)this->x, (float)this->y, (float)this->z); }
    operator TVector3<double>()                       const { return TVector3<double>((double)this->x, (double)this->y, (double)this->z); }
    const T operator [] (const unsigned int index)    const { return ((T*)this)[index]; }
          T operator [] (const unsigned int index)          { return ((T*)this)[index]; }
    TVector3<T>* operator *()                               { return ((TVector3<T>*)this); }
    const TVector3<T> operator -()                    const { return TVector3<T>(-x,-y,-z); }

    // vector assignment operators
    TVector3<T>& operator  = (const TVector3<T> &rhs) { this->Set(rhs); return *this; }
    TVector3<T>& operator += (const TVector3<T> &rhs) { this->x += rhs.x; this->y += rhs.y; this->z += rhs.z; return *this; }
    TVector3<T>& operator -= (const TVector3<T> &rhs) { this->x -= rhs.x; this->y -= rhs.y; this->z -= rhs.z; return *this; }
    TVector3<T>& operator *= (const TVector3<T> &rhs) { this->x *= rhs.x; this->y *= rhs.y; this->z *= rhs.z; return *this; }
    TVector3<T>& operator /= (const TVector3<T> &rhs) { this->x /= rhs.x; this->y /= rhs.y; this->z /= rhs.z; return *this; }

    // scalar assignment operators
    TVector3<T>& operator  = (const T rhs) { this->Set(rhs,rhs,rhs); return *this; }
    TVector3<T>& operator += (const T rhs) { this->x += rhs; this->y += rhs; this->z += rhs; return *this; }
    TVector3<T>& operator -= (const T rhs) { this->x -= rhs; this->y -= rhs; this->z -= rhs; return *this; }
    TVector3<T>& operator *= (const T rhs) { this->x *= rhs; this->y *= rhs; this->z *= rhs; return *this; }
    TVector3<T>& operator /= (const T rhs) { this->x /= rhs; this->y /= rhs; this->z /= rhs; return *this; }

    // binary vector operators
    const TVector3<T> operator + (const TVector3<T> &rhs) const { return TVector3<T>(*this) += rhs; }
    const TVector3<T> operator - (const TVector3<T> &rhs) const { return TVector3<T>(*this) -= rhs; }
    const TVector3<T> operator * (const TVector3<T> &rhs) const { return TVector3<T>(*this) *= rhs; }
    const TVector3<T> operator / (const TVector3<T> &rhs) const { return TVector3<T>(*this) /= rhs; }

    // binary scalar operators
    const TVector3<T> operator + (const T rhs) const { return TVector3<T>(*this) += rhs; }
    const TVector3<T> operator - (const T rhs) const { return TVector3<T>(*this) -= rhs; }
    const TVector3<T> operator * (const T rhs) const { return TVector3<T>(*this) *= rhs; }
    const TVector3<T> operator / (const T rhs) const { return TVector3<T>(*this) /= rhs; }

    TVector3<T>& Normalize()                                  { *this /= (T)this->Magnitude(); return *this; }

    double Sum()                                const { return this->x + this->y + this->z; }
    double Dot(const TVector3<T> &v)            const { return TVector3<T>((*this) * v).Sum(); }
    double SquareMag()                          const { return this->Dot(*this); }
    double Magnitude()                          const { return sqrt(this->SquareMag()); }
    double SquareDelta(const TVector3<T> &v)    const { return ((*this) - v).SquareMag(); }
    double Delta(const TVector3<T> &v)          const { return sqrt(this->SquareDelta(v)); }

    const TVector3<T> Midpoint(const TVector3<T> &v) const { return TVector3<T>((*this - v) / 2 + v); }
    const TVector3<T> Average(const TVector3<T> &v) const { return TVector3<T>((*this + v) / 2); }

    const TVector3<T> Cross(const TVector3<T> &v) const {
        return TVector3<T>( (y * v.z) - (z * v.y) , (z * v.x) - (x * v.z) , (x * v.y) - (y * v.x) );
    }

    const TVector3<T> NormalVector(const TVector3<T> &v2) {
        TVector3<T> n = TVector3<T>((*this).Cross(v2));
        return n.Normalize();
    }

    const TVector3<T> NormalVector(const TVector3<T> &v2, const TVector3<T> &v3) {
        TVector3<T> vx = TVector3<T>(v2 - (*this));
        TVector3<T> vy = TVector3<T>(v3 - (*this));
        return vx.NormalVector(vy);
    }
};

template<typename T> class TVector4 {
public:
    T x,y,z,w;

    TVector4()                                              { this->Set(0,0,0,0); }
    TVector4(const T x, const T y, const T z, const T w)    { this->Set(x,y,z,w); }
    TVector4(const T *lp)                                   { this->Set(lp); }
    TVector4(const TVector2<T> &v)                          { this->Set(v); }
    TVector4(const TVector3<T> &v)                          { this->Set(v); }
    TVector4(const TVector4<T> &v)                          { this->Set(v); }

    void Set(const T x, const T y, const T z, const T w)    { this->x = x; this->y = y; this->z = z; this->w = w; }
    void Set(const T *lp)                                   { this->Set(lp[0],lp[1],lp[2],lp[3]); }
    void Set(const TVector2<T> &v)                          { this->Set(v.x, v.y,   0,   0); }
    void Set(const TVector3<T> &v)                          { this->Set(v.x, v.y, v.z,   0); }
    void Set(const TVector4<T> &v)                          { this->Set(v.x, v.y, v.z, v.w); }

    // unary operators
    operator TVector4<float>()                        const { return TVector4<float>((float)this->x, (float)this->y, (float)this->z, (float)this->w); }
    operator TVector4<double>()                       const { return TVector4<double>((double)this->x, (double)this->y, (double)this->z, (double)this->w); }
    const T operator [] (const unsigned int index)    const { return ((T*)this)[index]; }
          T operator [] (const unsigned int index)          { return ((T*)this)[index]; }
    TVector4<T>* operator *()                               { return ((TVector4<T>*) this); }
    const TVector4<T> operator -()                    const { return TVector4<T>(-x,-y,-z,-w); }

    TVector4<T>& operator  = (const TVector4<T> &rhs) { this->Set(rhs); return *this; }
    TVector4<T>& operator += (const TVector4<T> &rhs) { this->x += rhs.x; this->y += rhs.y; this->z += rhs.z; this->w += rhs.w; return *this; }
    TVector4<T>& operator -= (const TVector4<T> &rhs) { this->x -= rhs.x; this->y -= rhs.y; this->z -= rhs.z; this->w -= rhs.w; return *this; }
    TVector4<T>& operator *= (const TVector4<T> &rhs) { this->x *= rhs.x; this->y *= rhs.y; this->z *= rhs.z; this->w *= rhs.w; return *this; }
    TVector4<T>& operator /= (const TVector4<T> &rhs) { this->x /= rhs.x; this->y /= rhs.y; this->z /= rhs.z; this->w /= rhs.w; return *this; }

    TVector4<T>& operator  = (const T rhs) { this->Set(rhs,rhs,rhs,rhs); return *this; }
    TVector4<T>& operator += (const T rhs) { this->x += rhs; this->y += rhs; this->z += rhs; this->w += rhs; return *this; }
    TVector4<T>& operator -= (const T rhs) { this->x -= rhs; this->y -= rhs; this->z -= rhs; this->w -= rhs; return *this; }
    TVector4<T>& operator *= (const T rhs) { this->x *= rhs; this->y *= rhs; this->z *= rhs; this->w *= rhs; return *this; }
    TVector4<T>& operator /= (const T rhs) { this->x /= rhs; this->y /= rhs; this->z /= rhs; this->w /= rhs; return *this; }

    const TVector4<T> operator + (const TVector4<T> &rhs) const { return TVector4<T>(*this) += rhs; }
    const TVector4<T> operator - (const TVector4<T> &rhs) const { return TVector4<T>(*this) -= rhs; }
    const TVector4<T> operator * (const TVector4<T> &rhs) const { return TVector4<T>(*this) *= rhs; }
    const TVector4<T> operator / (const TVector4<T> &rhs) const { return TVector4<T>(*this) /= rhs; }

    const TVector4<T> operator + (const T rhs) const { return TVector4<T>(*this) += rhs; }
    const TVector4<T> operator - (const T rhs) const { return TVector4<T>(*this) -= rhs; }
    const TVector4<T> operator * (const T rhs) const { return TVector4<T>(*this) *= rhs; }
    const TVector4<T> operator / (const T rhs) const { return TVector4<T>(*this) /= rhs; }

    TVector4<T>& Normalize()                                  { *this /= (T)this->Magnitude(); return *this; }

    double Sum() { return this->x + this->y + this->z + this->w; }
    double Dot(const TVector4<T> &b) { return TVector4<T>((*this) * b).Sum(); }
    double SquareMag() { return (*this).Dot(*this); }
    double Magnitude() { return sqrt(this->SquareMag()); }
    double SquareDelta(const TVector4<T> &b) { return ((*this) - b).SquareMag(); }
    double Delta(const TVector4<T> &b) { return sqrt(this->SquareDelta(b)); }

    const TVector4<T> Midpoint(const TVector4<T> &v) { return TVector4<T>((*this - v) / 2 + v); }
    const TVector4<T> Average(const TVector4<T> &v) { return TVector4<T>((*this + v) / 2); }
};

class CMatrix;

template <typename T> class TQuaternion : public TVector4<T> {
public:
    TQuaternion()                                              { this->Set(0,0,0,1); }
    TQuaternion(const T x, const T y, const T z, const T w)    { this->Set(x,y,z,w); }
    TQuaternion(const T *lp)                                   { this->Set(lp); }
    TQuaternion(const TVector2<T> &v)                          { this->Set(v); }
    TQuaternion(const TVector3<T> &v)                          { this->Set(v); }
    TQuaternion(const TVector4<T> &v)                          { this->Set(v); }
    TQuaternion(const TQuaternion<T> &q)                       { this->Set(q);}

    void Set(const T x, const T y, const T z, const T w)    { this->x = x; this->y = y; this->z = z; this->w = w; }
    void Set(const T *lp)                                   { this->Set(lp[0],lp[1],lp[2],lp[3]); }
    void Set(const TVector2<T> &v)                          { this->Set(v.x, v.y,   0,   0); }
    void Set(const TVector3<T> &v)                          { this->Set(v.x, v.y, v.z,   0); }
    void Set(const TVector4<T> &v)                          { this->Set(v.x, v.y, v.z, v.w); }
    void Set(const TQuaternion<T> &q)                       { this->Set(q.x,q.y,q.z,q.w); }

    TQuaternion<T>* operator *()                            { return ((TQuaternion<T>*) this); }
    const TQuaternion<T> operator * (const TQuaternion<T> &q) const {
	    // 12 muls, 30 adds
	    T E = (x + z)*(q.x + q.y);
	    T F = (z - x)*(q.x - q.y);
	    T G = (w + y)*(q.w - q.z);
	    T H = (w - y)*(q.w + q.z);
	    T A = F - E;
	    T B = F + E;
	    return TQuaternion<T>(
		    (T)( (w + x)*(q.w + q.x) + (A - G - H) * 0.5f ),
		    (T)( (w - x)*(q.y + q.z) + (B + G - H) * 0.5f ),
		    (T)( (y + z)*(q.w - q.x) + (B - G + H) * 0.5f ),
		    (T)( (z - y)*(q.y - q.z) + (A + G + H) * 0.5f ));
    }
    void operator *= (const TQuaternion<T> &rhs) { *this = *this * rhs; }

    void operator = (const CMatrix &m) {
	    // Check the sum of the diagonal
	    T tr = m(0, 0) + m(1, 1) + m(2, 2);
	    if (tr > 0.0f) {
		    // The sum is positive
		    // 4 muls, 1 div, 6 adds, 1 trig function call
		    T s = (T)sqrt(tr + 1.0f);
		    w = s * 0.5f;
		    s = 0.5f / s;
		    x = (m(1, 2) - m(2, 1)) * s;
		    y = (m(2, 0) - m(0, 2)) * s;
		    z = (m(0, 1) - m(1, 0)) * s;
	    } else {
		    // The sum is negative
		    // 4 muls, 1 div, 8 adds, 1 trig function call
		    const int nIndex[3] = {1, 2, 0};
		    int i, j, k;
		    i = 0;
		    if(m(1, 1) > m(i, i)) i = 1;
		    if(m(2, 2) > m(i, i)) i = 2;
		    j = nIndex[i];
		    k = nIndex[j];

		    T s = (T)sqrt((m(i, i) - (m(j, j) + m(k, k))) + 1.0f);
		    (*this)[i] = s * 0.5f;
		    if(s != 0.0) s = 0.5f / s;
		    (*this)[j] = (m(i, j) + m(j, i)) * s;
		    (*this)[k] = (m(i, k) + m(k, i)) * s;
		    (*this)[3] = (m(j, k) - m(k, j)) * s;
	    }
    }

    const TQuaternion<T> Conjugate() const { return TQuaternion<T>(-x,-y,-z,w); }
    const TQuaternion<T> UnitInverse() const { return this->Conjugate(); }
    const TQuaternion<T> Inverse() const { return this->UnitInverse() / (float32) TVector4<T>(*this).SquareMag(); }
    const TVector3<T> RotateVector(const TVector3<T> &v) { return TVector3<T>((T*)&TVector4<T>((*this) * TQuaternion<T>(v) * this->UnitInverse())); }
    const TVector4<T> RotateVector(const TVector4<T> &v) { return TVector4<T>((*this) * TQuaternion<T>(v) * this->UnitInverse()); }

    void SetAxisAngle(const TVector3<T> &vAxis, const T fAngle) {
		// 4 muls, 2 trig function calls
		T f = fAngle * 0.5f;
		this->Set(vAxis * (T)sin(f));
		w = (T)cos(f);
	}

	void GetAxisAngle(TVector3<T> &vAxis, T &fAngle) const {
		// 4 muls, 1 div, 2 trig function calls
		fAngle = (T)acos(w);
		vAxis = TVector3<T>(*this / (T)sin(fAngle));
		fAngle *= 2.0f;
	}

	void Rotate(const TQuaternion<T> &q)			{ *this = q * *this; }
	void Rotate(const TVector3<T> &vAxis, const T fAngle) {
		TQuaternion<T> q;
		q.SetAxisAngle(vAxis, fAngle);
		Rotate(q);
	}

    TVector3<T> GetViewAxis() const {
		// 6 muls, 7 adds
		T x2 = x + x, y2 = y + y, z2 = z + z;
		T xx = x * x2, xz = x * z2;
		T yy = y * y2, yz = y * z2;
		T wx = w * x2, wy = w * y2;
		return -TVector3<T>(xz+wy, yz-wx, 1-(xx+yy));
	}

	TVector3<T> GetUpAxis() const {
		// 6 muls, 7 adds
		T x2 = x + x, y2 = y + y, z2 = z + z;
		T xx = x * x2, xy = x * y2;
		T yz = y * z2, zz = z * z2;
		T wx = w * x2, wz = w * z2;
		return TVector3<T>(xy-wz, 1-(xx+zz), yz+wx);
	}

	TVector3<T> GetRightAxis() const {
		// 6 muls, 7 adds
		T x2 = x + x, y2 = y + y, z2 = z + z;
		T xy = x * y2, xz = x * z2;
		T yy = y * y2, zz = z * z2;
		T wy = w * y2, wz = w * z2;
		return TVector3<T>(1-(yy+zz), xy+wz, xz-wy);
	}

    // Spherical linear interpolation between two quaternions
    TQuaternion<T> Slerp(const TQuaternion<T> &q2, const float t) {
	    // Calculate the cosine of the angle between the two
	    float fScale0, fScale1;
	    double dCos = this->x * q2.x + this->y * q2.y + this->z * q2.z + this->w * q2.w;

	    // If the angle is significant, use the spherical interpolation
	    if((1.0 - abs(dCos)) > DELTA) {
		    double dTemp = acos(abs(dCos));
		    double dSin = sin(dTemp);
		    fScale0 = (float)(sin((1.0 - t) * dTemp) / dSin);
		    fScale1 = (float)(sin(t * dTemp) / dSin);
	    } else { // Else use the cheaper linear interpolation
		    fScale0 = 1.0f - t;
		    fScale1 = t;
	    }
	    if(dCos < 0.0) fScale1 = -fScale1;

	    // Return the interpolated result
	    return (*this * fScale0) + (q2 * fScale1);
    }
};

class CMatrix {
public:
	// This class uses column-major order, as used by OpenGL
	// Here are the ways in which the matrix values can be accessed:
	// | f11 f21 f31 f41 |   | f1[0] f1[4] f1[8]  f1[12] |   | f2[0][0] f2[1][0] f2[2][0] f2[3][0] |
	// | f12 f22 f32 f42 |   | f1[1] f1[5] f1[9]  f1[13] |   | f2[0][1] f2[1][1] f2[2][1] f2[3][1] |
	// | f13 f23 f33 f43 | = | f1[2] f1[6] f1[10] f1[14] | = | f2[0][2] f2[1][2] f2[2][2] f2[3][2] |
	// | f14 f24 f34 f44 |   | f1[3] f1[7] f1[11] f1[15] |   | f2[0][3] f2[1][3] f2[2][3] f2[3][3] |
	union {
		struct { float f11, f12, f13, f14, f21, f22, f23, f24, f31, f32, f33, f34, f41, f42, f43, f44; };
		float f1[16];
		float f2[4][4];
	};

	CMatrix()						     {}
	CMatrix(const float f)				 { *this = f; }
	CMatrix(const float *pf)			 { *this = pf; }
	CMatrix(const TQuaternion<float> &q) { *this = q; }

	// Init functions
	void ZeroMatrix() {
		f11 = f12 = f13 = f14 = f21 = f22 = f23 = f24 = f31 = f32 = f33 = f34 = f41 = f42 = f43 = f44 = 0;
	}
	void IdentityMatrix() {
		f12 = f13 = f14 = f21 = f23 = f24 = f31 = f32 = f34 = f41 = f42 = f43 = 0;
		f11 = f22 = f33 = f44 = 1;
	}

	operator       float* ()					        { return f1; }
	operator const float* ()                      const { return f1; }
	float &operator [] (const int n)					{ return f1[n]; }
	float  operator [] (const int n) const			    { return f1[n]; }
	float &operator () (const int i, const int j)		{ return f2[i][j]; }
	float  operator () (const int i, const int j) const { return f2[i][j]; }

	void operator = (const float f)					    { for(register int i=0; i<16; i++) f1[i] = f; }
	void operator = (const float *pf)					{ for(register int i=0; i<16; i++) f1[i] = pf[i]; }
    void operator = (const TQuaternion<float> &q) {
	    // 9 muls, 15 adds
	    float x2 = q.x + q.x, y2 = q.y + q.y, z2 = q.z + q.z;
	    float xx = q.x * x2, xy = q.x * y2, xz = q.x * z2;
	    float yy = q.y * y2, yz = q.y * z2, zz = q.z * z2;
	    float wx = q.w * x2, wy = q.w * y2, wz = q.w * z2;

	    f14 = f24 = f34 = f41 = f42 = f43 = 0; f44 = 1;
	    f11 = 1-(yy+zz);	f21 = xy+wz;		f31 = xz-wy;
	    f12 = xy-wz;		f22 = 1-(xx+zz);	f32 = yz+wx;
	    f13 = xz+wy;		f23 = yz-wx;		f33 = 1-(xx+yy);
    }

    CMatrix operator * (const CMatrix &m) const {
        // 36 muls, 27 adds
        // | f11 f21 f31 f41 |   | m.f11 m.f21 m.f31 m.f41 |   | f11*m.f11+f21*m.f12+f31*m.f13 f11*m.f21+f21*m.f22+f31*m.f23 f11*m.f31+f21*m.f32+f31*m.f33 f11*m.f41+f21*m.f42+f31*m.f43+f41 |
        // | f12 f22 f32 f42 |   | m.f12 m.f22 m.f32 m.f42 |   | f12*m.f11+f22*m.f12+f32*m.f13 f12*m.f21+f22*m.f22+f32*m.f23 f12*m.f31+f22*m.f32+f32*m.f33 f12*m.f41+f22*m.f42+f32*m.f43+f42 |
        // | f13 f23 f33 f43 | * | m.f13 m.f23 m.f33 m.f43 | = | f13*m.f11+f23*m.f12+f33*m.f13 f13*m.f21+f23*m.f22+f33*m.f23 f13*m.f31+f23*m.f32+f33*m.f33 f13*m.f41+f23*m.f42+f33*m.f43+f43 |
        // | 0   0   0   1   |   | 0     0     0     1     |   | 0                             0                             0                             1                                 |
        CMatrix mRet;
        mRet.f11 = f11*m.f11 + f21*m.f12 + f31*m.f13;
        mRet.f21 = f11*m.f21 + f21*m.f22 + f31*m.f23;
        mRet.f31 = f11*m.f31 + f21*m.f32 + f31*m.f33;
        mRet.f41 = f11*m.f41 + f21*m.f42 + f31*m.f43 + f41;
        mRet.f12 = f12*m.f11 + f22*m.f12 + f32*m.f13;
        mRet.f22 = f12*m.f21 + f22*m.f22 + f32*m.f23;
        mRet.f32 = f12*m.f31 + f22*m.f32 + f32*m.f33;
        mRet.f42 = f12*m.f41 + f22*m.f42 + f32*m.f43 + f42;
        mRet.f13 = f13*m.f11 + f23*m.f12 + f33*m.f13;
        mRet.f23 = f13*m.f21 + f23*m.f22 + f33*m.f23;
        mRet.f33 = f13*m.f31 + f23*m.f32 + f33*m.f33;
        mRet.f43 = f13*m.f41 + f23*m.f42 + f33*m.f43 + f43;
        mRet.f14 = mRet.f24 = mRet.f34 = 0; mRet.f44 = 1;
        return mRet;
    }
	void operator *= (const CMatrix &m)				{ *this = *this * m; }
	TVector3<float> operator * (const TVector3<float> &v) const	{ return TransformVector(v); }

	TVector3<float> TransformVector(const TVector3<float> &v) const {
		// 9 muls, 9 adds
		// | f11 f21 f31 f41 |   | v.x |   | f11*v.x+f21*v.y+f31*v.z+f41 |
		// | f12 f22 f32 f42 |   | v.y |   | f12*v.x+f22*v.y+f32*v.z+f42 |
		// | f13 f23 f33 f43 | * | v.z | = | f13*v.x+f23*v.y+f33*v.z+f43 |
		// | 0   0   0   1   |   | 1   |   | 1                           |
		return TVector3<float>( (f11*v.x+f21*v.y+f31*v.z+f41),
					            (f12*v.x+f22*v.y+f32*v.z+f42),
					            (f13*v.x+f23*v.y+f33*v.z+f43));
	}
	TVector3<float> TransformNormal(const TVector3<float> &v) const {
		// 9 muls, 6 adds
		// | f11 f21 f31 f41 |   | v.x |   | f11*v.x+f21*v.y+f31*v.z |
		// | f12 f22 f32 f42 |   | v.y |   | f12*v.x+f22*v.y+f32*v.z |
		// | f13 f23 f33 f43 | * | v.z | = | f13*v.x+f23*v.y+f33*v.z |
		// | 0   0   0   1   |   | 1   |   | 1                       |
		return TVector3<float>( (f11*v.x+f21*v.y+f31*v.z),
					            (f12*v.x+f22*v.y+f32*v.z),
					            (f13*v.x+f23*v.y+f33*v.z));
	}

	// Translate functions
	void TranslateMatrix(const float x, const float y, const float z) {
		// | 1  0  0  x |
		// | 0  1  0  y |
		// | 0  0  1  z |
		// | 0  0  0  1 |
		f12 = f13 = f14 = f21 = f23 = f24 = f31 = f32 = f34 = 0;
		f11 = f22 = f33 = f44 = 1;
		f41 = x; f42 = y; f43 = z;
	}
	void TranslateMatrix(const float *pf)		{ TranslateMatrix(pf[0], pf[1], pf[2]); }
	void Translate(const float x, const float y, const float z)	{
		// 9 muls, 9 adds
		// | f11 f21 f31 f41 |   | 1  0  0  x |   | f11 f21 f31 f11*x+f21*y+f31*z+f41 |
		// | f12 f22 f32 f42 |   | 0  1  0  y |   | f12 f22 f32 f12*x+f22*y+f32*z+f42 |
		// | f13 f23 f33 f43 | * | 0  0  1  z | = | f13 f23 f33 f13*x+f23*y+f33*z+f43 |
		// | 0   0   0   1   |   | 0  0  0  1 |   | 0   0   0   1                     |
		f41 = f11*x+f21*y+f31*z+f41;
		f42 = f12*x+f22*y+f32*z+f42;
		f43 = f13*x+f23*y+f33*z+f43;
	}
	void Translate(const float *pf)				{ Translate(pf[0], pf[1], pf[2]); }

	// Scale functions
	void ScaleMatrix(const float x, const float y, const float z) {
		// | x  0  0  0 |
		// | 0  y  0  0 |
		// | 0  0  z  0 |
		// | 0  0  0  1 |
		f12 = f13 = f14 = f21 = f23 = f24 = f31 = f32 = f34 = f41 = f42 = f43 = 0;
		f11 = x; f22 = y; f33 = z; f44 = 1;
	}
	void ScaleMatrix(const float *pf)			{ ScaleMatrix(pf[0], pf[1], pf[2]); }
	void Scale(const float x, const float y, const float z) {
		// 9 muls
		// | f11 f21 f31 f41 |   | x  0  0  0 |   | f11*x f21*y f31*z f41 |
		// | f12 f22 f32 f42 |   | 0  y  0  0 |   | f12*x f22*y f32*z f42 |
		// | f13 f23 f33 f43 | * | 0  0  z  0 | = | f13*x f23*y f33*z f43 |
		// | 0   0   0   1   |   | 0  0  0  1 |   | 0     0     0     1   |
		f11 *= x; f21 *= y; f31 *= z;
		f12 *= x; f22 *= y; f32 *= z;
		f13 *= x; f23 *= y; f33 *= z;
	}
	void Scale(const float *pf)					{ Scale(pf[0], pf[1], pf[2]); }

	// Rotate functions
	void RotateXMatrix(const float fRadians) {
		// | 1 0    0     0 |
		// | 0 fCos -fSin 0 |
		// | 0 fSin fCos  0 |
		// | 0 0    0     1 |
		f12 = f13 = f14 = f21 = f24 = f31 = f34 = f41 = f42 = f43 = 0;
		f11 = f44 = 1;

		float fCos = cosf(fRadians);
		float fSin = sinf(fRadians);
		f22 = f33 = fCos;
		f23 = fSin;
		f32 = -fSin;
	}
	void RotateX(const float fRadians) {
		// 12 muls, 6 adds, 2 trig function calls
		// | f11 f21 f31 f41 |   | 1 0    0     0 |   | f11 f21*fCos+f31*fSin f31*fCos-f21*fSin f41 |
		// | f12 f22 f32 f42 |   | 0 fCos -fSin 0 |   | f12 f22*fCos+f32*fSin f32*fCos-f22*fSin f42 |
		// | f13 f23 f33 f43 | * | 0 fSin fCos  0 | = | f13 f23*fCos+f33*fSin f33*fCos-f23*fSin f43 |
		// | 0   0   0   1   |   | 0 0    0     1 |   | 0   0                 0                 1   |
		float fTemp, fCos, fSin;
		fCos = cosf(fRadians);
		fSin = sinf(fRadians);
		fTemp = f21*fCos+f31*fSin;
		f31 = f31*fCos-f21*fSin;
		f21 = fTemp;
		fTemp = f22*fCos+f32*fSin;
		f32 = f32*fCos-f22*fSin;
		f22 = fTemp;
		fTemp = f23*fCos+f33*fSin;
		f33 = f33*fCos-f23*fSin;
		f23 = fTemp;
	}
	void RotateYMatrix(const float fRadians) {
		// | fCos  0 fSin  0 |
		// | 0     1 0     0 |
		// | -fSin 0 fCos  0 |
		// | 0     0 0     1 |
		f12 = f14 = f21 = f23 = f24 = f32 = f34 = f41 = f42 = f43 = 0;
		f22 = f44 = 1;

		float fCos = cosf(fRadians);
		float fSin = sinf(fRadians);
		f11 = f33 = fCos;
		f13 = -fSin;
		f31 = fSin;
	}
	void RotateY(const float fRadians) {
		// 12 muls, 6 adds, 2 trig function calls
		// | f11 f21 f31 f41 |   | fCos  0 fSin  0 |   | f11*fCos-f31*fSin f21 f11*fSin+f31*fCos f41 |
		// | f12 f22 f32 f42 |   | 0     1 0     0 |   | f12*fCos-f32*fSin f22 f12*fSin+f32*fCos f42 |
		// | f13 f23 f33 f43 | * | -fSin 0 fCos  0 | = | f13*fCos-f33*fSin f23 f13*fSin+f33*fCos f43 |
		// | 0   0   0   1   |   | 0     0 0     1 |   | 0                 0   0                 1   |
		float fTemp, fCos, fSin;
		fCos = cosf(fRadians);
		fSin = sinf(fRadians);
		fTemp = f11*fCos-f31*fSin;
		f31 = f11*fSin+f31*fCos;
		f11 = fTemp;
		fTemp = f12*fCos-f32*fSin;
		f32 = f12*fSin+f32*fCos;
		f12 = fTemp;
		fTemp = f13*fCos-f33*fSin;
		f33 = f13*fSin+f33*fCos;
		f13 = fTemp;
	}
	void RotateZMatrix(const float fRadians) {
		// | fCos -fSin 0 0 |
		// | fSin fCos  0 0 |
		// | 0    0     1 0 |
		// | 0    0     0 1 |
		f13 = f14 = f23 = f24 = f31 = f32 = f34 = f41 = f42 = f43 = 0;
		f33 = f44 = 1;

		float fCos = cosf(fRadians);
		float fSin = sinf(fRadians);
		f11 = f22 = fCos;
		f12 = fSin;
		f21 = -fSin;
	}
	void RotateZ(const float fRadians) {
		// 12 muls, 6 adds, 2 trig function calls
		// | f11 f21 f31 f41 |   | fCos -fSin 0 0 |   | f11*fCos+f21*fSin f21*fCos-f11*fSin f31 f41 |
		// | f12 f22 f32 f42 |   | fSin fCos  0 0 |   | f12*fCos+f22*fSin f22*fCos-f12*fSin f32 f42 |
		// | f13 f23 f33 f43 | * | 0    0     1 0 | = | f13*fCos+f23*fSin f23*fCos-f13*fSin f33 f43 |
		// | 0   0   0   1   |   | 0    0     0 1 |   | 0                 0                 0   1   |
		float fTemp, fCos, fSin;
		fCos = cosf(fRadians);
		fSin = sinf(fRadians);
		fTemp = f11*fCos+f21*fSin;
		f21 = f21*fCos-f11*fSin;
		f11 = fTemp;
		fTemp = f12*fCos+f22*fSin;
		f22 = f22*fCos-f12*fSin;
		f12 = fTemp;
		fTemp = f13*fCos+f23*fSin;
		f23 = f23*fCos-f13*fSin;
		f13 = fTemp;
	}
	void RotateMatrix(const TVector3<float> &v, const float f) {
		// 15 muls, 10 adds, 2 trig function calls
		float fCos = cosf(f);
		TVector3<float> vCos = v * (1 - fCos);
		TVector3<float> vSin = v * sinf(f);

		f14 = f24 = f34 = f41 = f42 = f43 = 0;
		f44 = 1;

		f11 = (v.x * vCos.x) + fCos;
		f21 = (v.x * vCos.y) - (vSin.z);
		f31 = (v.x * vCos.z) + (vSin.y);
		f12 = (v.y * vCos.x) + (vSin.z);
		f22 = (v.y * vCos.y) + fCos;
		f32 = (v.y * vCos.z) - (vSin.x);
		f13 = (v.z * vCos.x) - (vSin.y);
		f32 = (v.z * vCos.y) + (vSin.x);
		f33 = (v.z * vCos.z) + fCos;
	}
	void Rotate(const TVector3<float> &v, const float f) {
		// 51 muls, 37 adds, 2 trig function calls
		CMatrix mat;
		mat.RotateMatrix(v, f);
		*this *= mat;
	}

	void ModelMatrix(const TQuaternion<float> &q, const TVector3<float> &vFrom) {
		*this = q;
		f41 = vFrom.x;
		f42 = vFrom.y;
		f43 = vFrom.z;
	}
	void ViewMatrix(const TQuaternion<float> &q, const TVector3<float> &vFrom) {
		*this = q;
		f41 = -(vFrom.x*f11 + vFrom.y*f21 + vFrom.z*f31);
		f42 = -(vFrom.x*f12 + vFrom.y*f22 + vFrom.z*f32);
		f43 = -(vFrom.x*f13 + vFrom.y*f23 + vFrom.z*f33);
	}
	void ViewMatrix(const TVector3<float> &vFrom, const TVector3<float> &vView, const TVector3<float> &vUp, const TVector3<float> &vRight) {
		// 9 muls, 9 adds
		f11 = vRight.x;	f21 = vRight.y;	f31 = vRight.z;	f41 = -((float)vFrom.Dot(vRight));
		f12 = vUp.x;	f22 = vUp.y;	f32 = vUp.z;	f42 = -((float)vFrom.Dot(vUp));
		f13 = -vView.x;	f23 = -vView.y;	f33 = -vView.z;	f43 = -((float)vFrom.Dot(-vView));
		f14 = 0;		f24 = 0;		f34 = 0;		f44 = 1;
	}
	void ViewMatrix(const TVector3<float> &vFrom, const TVector3<float> &vAt, const TVector3<float> &vUp) {
		TVector3<float> vView = vAt - vFrom;
		vView.Normalize();
		TVector3<float> vRight = vView.Cross(vUp);
		vRight.Normalize();
		TVector3<float> vTrueUp = vRight.Cross(vView);
		vTrueUp.Normalize();
		ViewMatrix(vFrom, vView, vTrueUp, vRight);
	}
	void ProjectionMatrix(const float fNear, const float fFar, const float fFOV, const float fAspect) {
		// 2 muls, 3 divs, 2 adds, 1 trig function call
		float h = 1.0f / tanf((fFOV * 0.5f) * DEG2RAD);
		float Q = fFar / (fFar - fNear);
		f12 = f13 = f14 = f21 = f23 = f24 = f31 = f32 = f41 = f42 = f44 = 0;
		f11 = h / fAspect;
		f22 = h;
		f33 = Q;
		f34 = 1;
		f43 = -Q*fNear;
	}
    
    // For orthogonal matrices, I belive this also gives you the inverse.
	void Transpose() {
		float f;
		f = f12; f12 = f21; f21 = f;
        f = f13; f13 = f31; f31 = f;
        f = f14; f14 = f41; f41 = f;
        f = f23; f23 = f32; f32 = f;
        f = f24; f24 = f42; f42 = f;
        f = f34; f34 = f43; f43 = f;
	}
};