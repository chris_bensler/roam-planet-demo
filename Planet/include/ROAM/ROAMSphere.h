#pragma once
#include "ROAM.h"

namespace ROAM {

class CSphere : public TListNode<CSphere>, public C3DObject {
public:
    CVertexBuffer VertList;
    TLinkedList<CTriNode> TriList; // active triangles
    TLinkedList<CQuadNode> QuadList; // mergable quads
    Fractal::CSpectrum *Spectrum;
    uint16 Index[128*1024*3];
    uint32 TCount;

    CSphere() { this->Spectrum = NULL; TCount = 0; }
    ~CSphere() {}

    void Init(float32 scale);

    uint16 AddVertex(CVector3 v) { return this->VertList.Insert(v); }
    void DelVertex(uint16 index) { this->VertList.Remove(index); }

    bool Split(CTriNode* tri);
    void Merge(CQuadNode* quad);
    void Update(CVector position, CVector heading, float64 horizon, float64 max_error);

    uint16 Tesselate(uint16 iterations);
    void Simplify(uint16 iterations);

    bool SplitAll();
    void MergeAll();

    const CVector3f GetColor(float32 height) {
        if (this->Spectrum == NULL) return CVector3f(128,128,128);
        return this->Spectrum->GetGradient(height);
    }

private:
    void Reset() {
        this->QuadList.RemoveAll();
        this->TriList.RemoveAll();
        this->VertList.Reset();
    }
};

} // namespace ROAM
