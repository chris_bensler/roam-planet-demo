#pragma once
#include "Global.h"
#include "Math/Fractal.h"
#include "VertexBuffer.h"

//#define ROAM_DEBUG
#define MAX_ALTITUDE +100.0f //km

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                       //                                 //                                   //                       //
//                       //                                 //                                   //    +-------------+    //
//    +-------------+    //                0                //                 +                 //    | \   c0    / |    //
//    |           / |    //                +                //               / | \               //    |   \     /   |    //
//    | tri[0]  /   |    //              /   \              //             /   |   \             //    |     \ /     |    //
//    |       /     |    //            /       \            //           /     |     \           //    | r0   x   r1 |    //
//    |     /       |    //          /2         1\          //         /       |       \         //    |     / \     |    //
//    |   /  tri[1] |    //        /               \        //       /    root | child   \       //    |   /     \   |    //
//    | /           |    //      /         0         \      //     /           |           \     //    | /    c1   \ |    //
//    +-------------+    //  1 +-----------------------+ 2  //    +------------+------------+    //    +-------------+    //
//    A. Split Face      //          B. Triangle            //         C. Split Triangle         //   D. Merge Children   //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// A ROAM mesh begins as a grid of square faces where each face is comprised of two right triangles.
// The triangles are arranged on each face such that when viewing the front face, tri[0] is at the top-left and tri[1] is at the bottom right.
// See the first diagram above.
// 
// The vertices of each triangle are ordered ccw, beginning with vertex[0] at the right angle.
// The edges of each tri are also ordered ccw, beginning with edge[0] at the longest edge (opposite vertex[0]).

#ifndef _DEBUG
#undef ROAM_DEBUG
#endif

#include "ROAMPriority.h"
#include "ROAMTri.h"
#include "ROAMQuad.h"
#include "ROAMSphere.h"
