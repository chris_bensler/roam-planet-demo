#pragma once
#include "ROAM.h"

namespace ROAM {
class CTriNode;

// A mergable quad
class CQuadNode : public TListNode<CQuadNode>, public CPriority {
public:
    CTriNode *parent; // pointer to one of the two parent triangles that form the merged quad.

    CQuadNode() {}
    CQuadNode(CTriNode *tri);
    ~CQuadNode();
};

} // namespace ROAM
