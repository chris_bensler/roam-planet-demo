#pragma once
#include "ROAM.h"

namespace ROAM {

class CPriority {
protected:
    uint8 priority;
public:
    CVector midpoint;
    CVector target;
    float64 height;
    float64 offset;
    float64 length; // squared length of the longest edge
    //float32 delta; // deviation from the midpoint of the longest edge to the expected vector

    CPriority() {
        priority = 0;
        midpoint = NULL;
        target = NULL;
        height = 0;
        offset = 0;
        length = 0;
    }

    uint8 GetWait() { if (priority) return priority--; else return 0; }
    void SetWait(uint8 p) { priority = p; }

    float64 GetError(const CVector &position, const CVector &heading, const float64 horizon) {
      float64 distance = position.SquareDelta(this->target);
      float64 fov = heading.Dot(CVector(this->target - position).Normalize());
        if ((distance < this->length) || ((distance < horizon) && (fov > (1-FOVY*RADIANS)))) {
            return (this->offset / (sqrt(distance) / fov));// * sqrt(this->length);
        }
        return 0.0f;
    }

    void CopyPriority(CPriority *src) {
        this->midpoint = CVector(src->midpoint);
        this->target = CVector(src->target);
        this->height = src->height;
        this->offset = src->offset;
        this->length = src->length;
    }
};

} // namespace ROAM
