#pragma once
#include "ROAM.h"

namespace ROAM {
class CQuadNode;
class CSphere;

// A right triangle
class CTriNode : public TListNode<CTriNode>, public CPriority {
public:
    uint16 vert[3]; // face[0] is always the vertex at the right angle
    CTriNode *parent;
    CTriNode *edge[3]; // pointers to neighbouring triangles
    CQuadNode *quad; // pointer to the quad this triangle belongs to. NULL if none
    uint16 lod; // level of detail/depth

    CTriNode() { this->Init(NULL,0,0,0); }
    CTriNode(CTriNode *tri, uint16 v0, uint16 v1, uint16 v2) {
        this->Init(tri,v0,v1,v2);
    }
    ~CTriNode() {
        vert[0] = vert[1] = vert[2] = NULL;
        parent = NULL;
        edge[0] = edge[1] = edge[2] = NULL;
        quad = NULL;
        lod = -1;
    }

    void SetVerts(uint16 v0, uint16 v1, uint16 v2) {
        this->vert[0] = v0; this->vert[1] = v1; this->vert[2] = v2;
    }

    void SetEdges(CTriNode *t0, CTriNode *t1, CTriNode *t2) {
        this->edge[0] = t0; this->edge[1] = t1; this->edge[2] = t2;
    }

    void ReplaceEdge(CTriNode *triOld, CTriNode *triNew) {
        for (int i=0; i < 3; i++) {
            if (this->edge[i] == triOld) this->edge[i] = triNew;
        }
    }

    CVector GetMidPoint(CSphere *sphere);

    bool CanSplit() { return this->edge[0]->edge[0] == this; }

    // if it's mergable, walking the edges in 1 direction 4x will point back to this tri
    // additionally, check that all four triangles share the same vert[0] (the midpoint of the quad)
    bool CanMerge() {
        CTriNode *t = this;
        uint16 v = t->vert[0];
        for (int i=0; i < 3; i++) {
            t = t->edge[1];
            if (v != t->vert[0]) return false;
        }
        return (t->edge[1] == this);
    }

    void UpdatePriority(CSphere *sphere);
    void CopyPriority(CSphere *sphere);

    CVector3 GetNormal(CSphere *sphere);
    void UpdateNormal(CSphere *sphere, uint16 index);
    void UpdateNormals(CSphere *sphere) {
        for (int i=0; i < 3; i++) this->UpdateNormal(sphere,i);
    }

private:
    void Init(CTriNode *tri, uint16 v0, uint16 v1, uint16 v2) {
        this->vert[0] = v0; this->vert[1] = v1; this->vert[2] = v2;
        this->edge[0] = this->edge[1] = this->edge[2] = NULL;
        this->parent = tri;
        this->quad = NULL;
        if (tri != NULL) this->lod = tri->lod+1; else this->lod = 0;
    }
};

} // namespace ROAM
