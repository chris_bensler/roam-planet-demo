#pragma once
#include "Global.h"

class CVertex {
public:
    CVector3 pos;
    CVector3 normal;
    CVector3f rgb;
    CVector2f uv;

    CVertex() {}
    CVertex(const CVertex &v) { *this = v; }

    void operator =(const CVertex &v) {
        pos = CVector3(v.pos);
        normal = CVector3(v.normal);
        rgb = CVector3f(v.rgb);
        uv = CVector2f(v.uv);
    }
};