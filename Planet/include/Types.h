#pragma once
#include "SFML/Graphics.hpp"
#include "Vector.h"

typedef sf::Uint8  uint8;
typedef sf::Uint16 uint16;
typedef sf::Uint32 uint32;

typedef sf::Int8  int8;
typedef sf::Int16 int16;
typedef sf::Int32 int32;

typedef float  float32;
typedef double float64;

typedef TVector2<int32>   CVector2i;
typedef TVector2<uint32>  CVector2u;
typedef TVector2<float32> CVector2f;
typedef TVector2<float64> CVector2d;

typedef TVector3<int32>   CVector3i;
typedef TVector3<uint32>  CVector3u;
typedef TVector3<float32> CVector3f;
typedef TVector3<float64> CVector3d;

typedef TVector4<int32>   CVector4i;
typedef TVector4<uint32>  CVector4u;
typedef TVector4<float32> CVector4f;
typedef TVector4<float64> CVector4d;

typedef TVector3<uint16>  CFace;

typedef CVector2i CVector2;
typedef CVector3f CVector3;
typedef CVector4f CVector4;

typedef CVector3d CVector;

typedef TQuaternion<float32> CQuaternion32;
typedef TQuaternion<float64> CQuaternion64;

typedef CQuaternion32 CQuaternion;
