#include "Math/Noise.h"

int ClassicNoise::grad3[12][3] = {0};
int ClassicNoise::perm[512] = {0};

int SimplexNoise::grad3[12][3] = {0};
int SimplexNoise::grad4[32][4] = {0};
int SimplexNoise::perm[512] = {0};
int SimplexNoise::simplex[64][4] = {0};

